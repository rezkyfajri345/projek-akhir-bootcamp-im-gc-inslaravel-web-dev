<h1 align="center">Selamat datang di Aplikasi Sistem Informasi Sekolah 👋</h1>

# Sistem Informasi Sekolah

Sistem informasi sekolah berbasis laravel 8 dengan template dashboard
[Stisla](https://getstisla.com/)

<p align="center">Dibuat Menggunakan Framework Laravel Versi 8</p>


## Fitur apa saja yang tersedia di Sistem Informasi Sekolah?

- CRUD Jurusan
- CRUD Mata Pelajaran
- CRUD Guru
- CRUD Kelas
- CRUD User
- CRUD Materi
- CRUD Tugas & Jawaban
- CRUD Jadwal Sekolah


## Akun Default

- email: admin@mail.com
- Password: admin123

---

## Install

```bash
composer install
cp .env.example .env
```

**Buka `.env` lalu ubah baris berikut sesuai dengan databasemu yang ingin dipakai**

```bash
DB_PORT=3306
DB_DATABASE=projectakhir
DB_USERNAME=root
DB_PASSWORD=
```

**Instalasi Aplikasi**

```bash
php artisan key:generate
php artisan migrate --seed
```

**Jalankan Aplikasi**

```bash
php artisan serve
```

## Author

Bootcamp Kelompok 3
-REZKY FAJRI
-NELLY PUTRI WARUWU
-TRI LOVELY PURBA
-MASITA AMALIA

## ERD


<img src="https://i.ibb.co/TMPpMcq/erd-sistem-informasi-siswa.png" >

## Demo Video

Link 
https://drive.google.com/file/d/1nClyCbN1IOoCHt7PmhvrfYmTKNKNaLPr/view?usp=sharing
